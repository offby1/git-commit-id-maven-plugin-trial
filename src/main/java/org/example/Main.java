package org.example;

import java.io.IOException;
import java.util.Properties;

public class Main {
    public static void main(String[] args) throws IOException {
        var props = new Properties();
        ClassLoader classLoader = Main.class.getClassLoader();

        var input = classLoader.getResourceAsStream("git.properties");
        if (input == null) {
            System.out.println("I guess maybe you're not running a jar or something");
        } else {
            props.load(input);
            System.out.printf("Hello world from %s\n", props.getProperty("git.commit.id.abbrev"));
        }
    }
}
